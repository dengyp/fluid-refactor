### Candidate Chinese Name:
* 
邓易平 
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 
个人认为本题主要考察如何在保证向下兼容的前提下，如何运用设计模式对已有代码进行扩展和抽象
思路：
1、通过阅读JmsMessageBrokerSupport代码，分析出代码中与具体JMS厂商实现有关和无关的两部分代码

2、视JmsMessageBrokerSupport为工厂模式中的工厂类，针对上一步分析结果，将与具体JMS厂商实现有关的代码转移到具体实现类中（如本例中的ActiveMq）
其中涉及到对原有实现的属性、方法的一些可见性的调整

3、将上面拆分出来的第一个产品类（ActiveMq）中的方法，统一声明到接口类（JmsMessageBroker）中，因为JmsMessageBrokerSupport提供了JmsMessageBroker中接口的默认实现，固也修改成实现该接口的具体类（同时还是我们的工厂类）

4、因为要保持向下兼容的缘故，所以我们的产品类还需要继承JmsMessageBrokerSupport以保证兼容性

5、至此，后续添加或者切换其他JMS产品时，结合自身实现JmsMessageBroker接口，再继承JmsMessageBrokerSupport即可


- - -