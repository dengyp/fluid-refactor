package com.paic.arch.jmsbroker.factory;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;

import com.paic.arch.jmsbroker.JmsMessageBroker;
import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;

public class ActiveMq extends JmsMessageBrokerSupport implements JmsMessageBroker {
	
	private BrokerService brokerService;
	
	public ActiveMq(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
    }
	 
	@Override
	public void createEmbeddedBroker() throws Exception {
		brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }
	
	@Override
	public void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }
	
	@Override
	public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }
	
	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }
	
	private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }
}
