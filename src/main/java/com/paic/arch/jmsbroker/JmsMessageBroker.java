package com.paic.arch.jmsbroker;

public interface JmsMessageBroker {

	void createEmbeddedBroker() throws Exception;
	
	void startEmbeddedBroker() throws Exception;
	
	void stopTheRunningBroker() throws Exception;
	
	long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;
}
