package com.paic.arch.jmsbroker;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerSupport JMS_SUPPORT;
    private static String REMOTE_BROKER_URL;

    @BeforeClass
    public static void setup() throws Exception {
        JMS_SUPPORT = JmsMessageBrokerSupport.createARunningEmbeddedBrokerOnAvailablePort();
        REMOTE_BROKER_URL = JMS_SUPPORT.getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
        JMS_SUPPORT.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        JmsMessageBrokerSupport.bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        
        // active mq
        /*getInstanceOfBrokerSupport("active")
        .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);*/
        
        // ibm mq
        /*getInstanceOfBrokerSupport("ibm")
        .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);*/
        
        // tibco mq
        /*getInstanceOfBrokerSupport("tibco")
        .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);*/
        long messageCount = JMS_SUPPORT.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = getInstanceOfBrokerSupport("active")
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        
        // active mq
        /*receivedMessage = getInstanceOfBrokerSupport("active")
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);*/
        
        // ibm mq
        /*receivedMessage = getInstanceOfBrokerSupport("ibm")
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);*/
        
        // tibco mq
        /*receivedMessage = getInstanceOfBrokerSupport("tibco")
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);*/
        
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
        System.out.println(receivedMessage + "==" + MESSAGE_CONTENT);
    }

    @Test(expected = JmsMessageBrokerSupport.NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        JmsMessageBrokerSupport.bindToBrokerAtUrl(REMOTE_BROKER_URL).retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }
    
    private JmsMessageBrokerSupport getInstanceOfBrokerSupport(String type) throws Exception {
    	JmsMessageBrokerSupport instance = null;
    	
    	if ("IBM".equalsIgnoreCase(type)) {
    		instance = JmsMessageBrokerSupport.bindToIbmMqBrokerAt(REMOTE_BROKER_URL);
    	} else if ("tibco".equalsIgnoreCase(type)) {
    		instance = JmsMessageBrokerSupport.bindToTibcoMqBrokerAt(REMOTE_BROKER_URL);
    	} else {
    		instance = JmsMessageBrokerSupport.bindToActiveMqBrokerAt(REMOTE_BROKER_URL);
    	}
    	return instance;
    }


}